from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):
    is_customer = models.BooleanField(default=False)
    is_lift = models.BooleanField(default=False)
    is_supplier = models.BooleanField(default=False)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    location = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)


class Lift(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    designation = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)

class Supplier(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
	location = models.CharField(max_length=100)
	phone_number = models.CharField(max_length=20)
