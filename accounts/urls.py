from django.urls import path, reverse_lazy
from . import views
from products.views import CustomerListView, SupplierListView
from products.views import confirm_delivery, view_orders
from django.contrib.auth.views import LogoutView

urlpatterns = [
    #path("", include("django.contrib.auth.urls")),
    path("../", views.home, name='home_page'),
    path("customer/", views.index, name="customer_index"),
    path("lift/", views.index, name="lift_index"),
    path("lift/<int:id>/", confirm_delivery, name="confirm_delivery"),
    path("supplier/", views.index, name="supplier_index"),
    path('register/', views.register, name='register'),
    path('customer_register/', views.customer_register.as_view(), name='customer_register'),
    path('lift_register/', views.lift_register.as_view(), name='lift_register'),
    path('supplier_register/', views.supplier_register.as_view(), name='supplier_register'),
    path("customer/order/", CustomerListView.as_view(), name="customer_order"),
    path("customer/cart/", view_orders, name='view_orders'),
    path("supplier/update/", SupplierListView.as_view(), name="supplier_update"),
    path("customer/", views.index, name='customer_index'),
    path('login/', views.login_request, name='login'),
    path('logout/', LogoutView.as_view(next_page=reverse_lazy('home_page')), name='logout'),
    path('profile/', views.get_user_profile, name ='profile'),
    path('password/', views.change_password, name = 'change_password'),
]
