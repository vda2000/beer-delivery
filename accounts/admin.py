from django.contrib import admin
from .models import User, Customer, Lift, Supplier

# Register your models here.

admin.site.register(User)
admin.site.register(Customer)
admin.site.register(Lift)
admin.site.register(Supplier)
