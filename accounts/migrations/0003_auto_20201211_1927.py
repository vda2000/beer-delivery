# Generated by Django 3.1.4 on 2020-12-11 12:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20201211_1914'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='is_supplier',
        ),
        migrations.AddField(
            model_name='supplier',
            name='phone_number',
            field=models.CharField(default='012345678', max_length=20),
        ),
    ]
