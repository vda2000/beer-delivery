from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.views.generic import CreateView
from django.contrib import messages
from .form import CustomerSignUpForm, LiftSignUpForm, SupplierSignUpForm
from django.contrib.auth.forms import AuthenticationForm
from .models import User, Lift
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from products.models import Order

# Create your views here.
def register(request):
    return render(request, 'register.html')

class user_register(CreateView):
	model = User

class customer_register(user_register):
    form_class = CustomerSignUpForm
    template_name = 'customer_register.html'

    def form_valid(self, form):
        user = form.save()
        #login(self.request, user)
        return render(self.request, 'login.html')


class lift_register(user_register):
    form_class = LiftSignUpForm
    template_name = 'lift_register.html'

    def form_valid(self, form):
        user = form.save()
        #login(self.request, user)
        return render(self.request, 'login.html')

class supplier_register(user_register):
    form_class = SupplierSignUpForm
    template_name = 'supplier_register.html'

    def form_valid(self, form):
        user = form.save()
        #login(self.request, user)
        return render(self.request, 'login.html')

def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                if user.is_customer:
                	return HttpResponseRedirect(reverse('customer_index'))
                elif user.is_lift:
                	return HttpResponseRedirect(reverse('lift_index'))
                else:
                	return HttpResponseRedirect(reverse('supplier_index'))
            else:
                messages.error(request, "Invalid username or password")
        else:
            messages.error(request, "Not existing this user")
    return render(request, 'login.html',
                  context={'form': AuthenticationForm()})


def logout_view(request):
    logout(request)
    #return redirect('/')
    #return render(request, "index.html")
    return HttpResponseRedirect(reverse('login'))

def index(request):
    if request.user.is_authenticated:
        if request.user.is_customer:
            return render(request, 'customer_index.html')
        elif request.user.is_lift:
            deliverer = Lift.objects.get(user=request.user)
            orders = Order.objects.filter(deliverer=deliverer)
            delivered = orders.filter(status=True).count()
            pending = orders.filter(status=False).count()
            context = {
                "orders": orders,
                "delivered": delivered, 
                "pending": pending
            }
            return render(request, 'lift_index.html', context)
        else:
        	return render(request, 'supplier_index.html')

    return render(request, 'index.html')

def get_user_profile(request):
    username = request.user.get_username()
    user = User.objects.get(username=username)
    return render(request, 'user_profile.html', {"user":user})

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            return redirect('/')
        else:
            messages.error(request, 'Incorrect old password or new passwords do not  match')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {
        'form': form
    })
    
def home(request):
    render(request, "index.html")