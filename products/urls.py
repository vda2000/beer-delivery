from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('beers/', views.BeerListView.as_view(), name='beers'),
    path('beers/<int:pk>', views.BeerDetailView.as_view(), name='beer-detail'),
    path('beer/<int:pk>/renew/', views.order, name='beer-order'),
    path('beer/<int:pk>/inex/', views.export_import, name='beer-import-export'),
    path('beers/<int:pk>', views.BeerDetailView.as_view(), name='beer-detail'),
    path('beer/<int:pk>/<int:total_price>', views.bill, name='bill'),
    path('beer/<int:id>', views.confirm_delivery, name='confirm_delivery')
]
