from django.db import models
from django.urls import reverse  # Used to generate URLs by reversing the URL patterns
import uuid
from accounts.models import Customer, Lift, User


# Create your models here.
class Beer(models.Model):
    """Model representing a book (but not a specific copy of a book)."""
    name = models.CharField(max_length=200)

    # Foreign Key used because book can only have one author, but authors can have multiple books
    # Author as a string rather than object because it hasn't been declared yet in the file
    supplier = models.ForeignKey('Supplier', on_delete=models.SET_NULL, null=True)

    summary = models.TextField(max_length=1000, help_text='Enter a brief description of this production')
    country = models.CharField('country', max_length=20)
    price = models.IntegerField(default=1000)
    count = models.IntegerField()

    def __str__(self):
        """String for representing the Model object."""
        return self.name

    def get_absolute_url(self):
        """Returns the url to access a detail record for this book."""
        return reverse('beer-detail', args=[str(self.id)])

    def get_customer_url(self):
        """Returns the url to access a detail record for this book."""
        return reverse('beer-order', args=[str(self.id)])

    def get_supplier_url(self):
        return reverse('beer-import-export', args=[str(self.id)])

class Supplier(models.Model):
    name = models.CharField(max_length=100)

    def get_absolute_url(self):
        return reverse('supplier-detail', args=[str(self.id)])

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

class Order(models.Model):
    
    user = models.ForeignKey(User, on_delete = models.CASCADE) 
    product_name = models.CharField(max_length=100, default="Not-a-beer")
    quantity = models.IntegerField(default=0)
    #order_items = models.ForeignKey(Order_Items, on_delete=models.CASCADE)
    address = models.CharField(max_length = 100)
    phone_number = models.CharField(max_length=20)
    total = models.DecimalField( max_digits = 20, decimal_places = 2 )
    deliverer = models.ForeignKey(Lift, on_delete=models.CASCADE)
    #note = models.CharField(max_length = 200, default="none")
    #payment = models.PositiveSmallIntegerField(choices=payment_choice, default=1)
    status = models.BooleanField(default=False)

    def confirm_delivery(self):
        self.status = True
        self.save()


    def __str__(self):
        return f"{self.total}, {self.status}"
