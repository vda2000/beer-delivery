import datetime
import random
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views import generic
from products.models import Beer, Supplier
from products.forms import OrderForm, ImportExportForm, Bill
from accounts.models import Lift
from .models import Order
from django.contrib import messages

def index(request):
    """View function for home page of site."""

    # Generate counts of some of the main objects
    num_beers = Beer.objects.all().count()

    # The 'all()' is implied by default.
    num_suppliers = Supplier.objects.count()
    context = {
        'num_beers': num_beers,
        'num_suppliers': num_suppliers,
    }

    # Render the HTML template index.html with the data in the context variable
    return render(request, 'products_index.html', context=context)


class BeerListView(generic.ListView):
    model = Beer
    # your own name for the list as a template variable
    context_object_name = 'beer_list'
    queryset = Beer.objects.all()
    template_name = 'beer_list.html'  # Specify your own template name/location

    def get_queryset(self):
        return Beer.objects.all()

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get the context
        context = super(BeerListView, self).get_context_data(**kwargs)
        # Create any data and add it to the context
        context['some_data'] = 'This is just some data'
        return context

class CustomerListView(BeerListView):
    template_name = 'customer_list.html'

class SupplierListView(BeerListView):
    template_name = 'supplier_list.html'

class BeerDetailView(generic.DetailView):
    model = Beer
    template_name = 'beer_detail.html'

    def beer_detail_view(request, primary_key):
        beer = get_object_or_404(Book, pk=primary_key)
        return render(request, 'beer_detail.html', context={'beer': beer})


@login_required
def order(request, pk):
    """View function for renewing a specific BookInstance by librarian."""
    beer = get_object_or_404(Beer, pk=pk)
    if not request.user.is_customer:
        return HttpResponse("This function is designated to customer only")

    # If this is a POST request then process the Form data
    if request.method == 'POST':

        # Create a form instance and populate it with data from the request (binding):
        form = OrderForm(request.POST)

        # Check if the form is valid:
        if form.is_valid():
            total_price = form.cleaned_data.get('order_count') * beer.price
            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)

            # redirect to a new URL:
            #return render(request, 'total.html', context)
            return HttpResponseRedirect(reverse('bill', args=(pk, total_price, )))

    # If this is a GET (or any other method) create the default form.
    else:
        form = OrderForm()
    context = {
        'form': form,
        'beer': beer,
    }

    return render(request, 'beer-order.html', context)

@login_required
def bill(request, pk, total_price):
    beer = get_object_or_404(Beer, pk=pk)

    if not request.user.is_customer:
        return HttpResponse("This function is designated to customer only")

    # If this is a POST request then process the Form data
    if request.method == 'POST':
       bill_form = Bill(request.POST)
       if bill_form.is_valid():
            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)

            # redirect to a new URL:
            #return render(request, 'total.html', context)
            #total_price
            address = bill_form.cleaned_data.get('address')
            phone_number = bill_form.cleaned_data.get('phone_number')
            n =  random.randint(Lift.objects.all().first().pk ,  Lift.objects.all().last().pk)
            deliverer = Lift.objects.get(user__id=n)
            order = Order.objects.create(user=request.user ,address=address, phone_number=phone_number, total = total_price, deliverer=deliverer, product_name=beer.name, quantity=total_price/beer.price)
            #return HttpResponse("Your order has been taken successfully")
            messages.success(request,"Order has been placed. It will be delivered soon.")
    # If this is a GET (or any other method) create the default form.
    else:
        bill_form = Bill()
    context = {'total_price': total_price, 'form': bill_form}
    return render(request, 'bill.html', context)

@login_required
def export_import(request, pk):
    """View function for renewing a specific BookInstance by librarian."""
    beer = get_object_or_404(Beer, pk=pk)

    if request.user.is_customer or request.user.is_lift:
        return HttpResponse("This function is designated to supplier only")
    # If this is a POST request then process the Form data
    if request.method == 'POST':

        # Create a form instance and populate it with data from the request (binding):
        form = ImportExportForm(request.POST)

        # Check if the form is valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)

            # redirect to a new URL:
            return HttpResponse("Your fluctuation has been taken successfully.")

    # If this is a GET (or any other method) create the default form.
    else:
        form = ImportExportForm()
    context = {
        'form': form,
        'beer': beer,
    }

    return render(request, 'beer-import-export.html', context)

def confirm_delivery(request, id):
    if request.user.is_lift:
        order = Order.objects.get(id=id)
        order.status = True
        order.save()
        return JsonResponse({"success":True})

def view_orders(request):
    if request.user.is_customer:
        orders = Order.objects.filter(user=request.user)
        context = {
            "orders": orders
        }
        return render(request, "view_orders.html", context)