from django.contrib import admin

# Register your models here.
from .models import Beer, Supplier, Order

admin.site.register(Beer)
admin.site.register(Supplier)
admin.site.register(Order)