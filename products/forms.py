import datetime

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class OrderForm(forms.Form):
    order_count = forms.IntegerField(help_text="Enter number of beers you wish to order: ")
    #address = forms.CharField(help_text = "Enter your address: ")
    #phone_number = forms.CharField(help_text = "Enter your phone number: ")

    def clean_order_count(self):
        data = self.cleaned_data['order_count']
        return data
class Bill(forms.Form):
	address = forms.CharField(help_text = "Enter your address: ")
	phone_number = forms.CharField(help_text = "Enter your phone number: ")

class ImportExportForm(forms.Form):
	order_out = forms.IntegerField(help_text="Enter number of beers you wish to export: ")
	order_in = forms.IntegerField(help_text="Enter number of beers you wish to import: ")

	def clean_import_export(self):
		export_data = self.cleaned_data['order_out']
		import_data = self.cleaned_data['order_in']
		return (import_data, export_data)